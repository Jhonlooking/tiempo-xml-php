<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- CSS only -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
  <title>Document</title>
</head>

<body>
  <div class="container">
    <p class="fs-2">El Tiempo en su Pueblo</p>

    <!-- Selección de población -->
    <div class="row mb-3 col-5 offset-3">
      <form action="" method="POST">
        <select class="form-select mb-3" name="localidad">
          <option selected>Selecione Localidad</option>
          <option value="alicante">Alicante</option>
          <option value="campello">El Campello</option>
          <option value="lorca">Lorca</option>
        </select>
        <input type="submit" class="btn btn-light" value="Ver El tiempo">
      </form>
    </div>



    <?php

    // Definir variables
    $origen = "";
    $fecha = "";
    $dia = "";
    $productor = "";
    $xml ="";
    $localidad = "";
    $temMax = "";
    $temMin = "";

    // Seleccionar localidad
    if (isset($_POST["localidad"])) {
      $localidad = $_POST["localidad"];
      switch ($localidad) {
        case "alicante":
          $url = "http://www.aemet.es/xml/municipios/localidad_03014.xml";
          break;
        case "campello":
          $url = "http://www.aemet.es/xml/municipios/localidad_03050.xml";
          break;
        case "lorca":
          $url = "http://www.aemet.es/xml/municipios/localidad_30024.xml";
          break;
        default:
          echo "";
      }

      // leer XML de la URL
      $xml = simplexml_load_file($url);

      // Asignar variables con el contenido de las etiquetas XML
      $origen = $xml->nombre;
      $fecha = $xml->elaborado;
      $dia = $xml->prediccion->dia['fecha'];
      $productor =  $xml->origen->productor;


      foreach ($xml->prediccion->dia as $ele) {
        $temMax = $ele->temperatura->maxima;
        $temMin =  $ele->temperatura->minima;
        $sensMax = $ele->sens_termica->maxima;
        $sensMin = $ele->sens_termica->minima;

        break;
      }
    }
    ?>
    <div class="row">
      <h5 class="card-title">Tiempo en <?= $origen . " el día " . $dia ?></h5>

      <!-- Tarjeta 'Temperaturas' -->
      <div class="card" style="width: 18rem;">
        <ul class="list-group list-group">
          <li class="list-group-item active" aria-current="true">Temperaturas</li>
          <li class="list-group-item d-flex justify-content-between align-items-start">
            <div class="ms-2 me-auto">
              <div class="fw-bold">Temperatura</div>
              Máxima
            </div>
            <span class="badge bg-primary rounded-pill"><?= $temMax ?>&nbsp;Cº</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-start">
            <div class="ms-2 me-auto">
              <div class="fw-bold">Temperatura</div>
              Mínima
            </div>
            <span class="badge bg-primary rounded-pill"><?= $temMin ?>&nbsp;Cº</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-start">
            <div class="ms-2 me-auto">
              <div class="fw-bold">
                <?php
                if($localidad != "") {
                  foreach ($xml->prediccion->viento as $viento) {
                    $aire = $viento["periodo"];
                    echo $viento->direccion;
                  }
                }
                ?>
              </div>
            </div>
            <span class="badge bg-primary rounded-pill">14</span>
          </li>
        </ul>
      </div>

      <!-- Tarjeta VIENTO -->
      <div class="card" style="width: 18rem;">
        <ul class="list-group list-group">
          <li class="list-group-item active" aria-current="true">Viento</li>
          <?php
          if($localidad != "") {
            foreach ($xml->prediccion->dia->viento as $viento) {
              if ($viento->direccion != "") {
                echo '<li class="list-group-item d-flex justify-content-between align-items-start">';
                echo '<div class="ms-2 me-auto">';
                echo "<div class='fw-bold'>Periodo" . $viento['periodo'] . "</div>";
                echo "</div>";
                echo '<span class="badge bg-primary rounded-pill">' . $viento->velocidad . '&nbsp;Km/h</span>';
                echo '<span class="badge bg-primary rounded-pill">' . $viento->direccion . '</span></li>';
              }
            }
          }
          ?>
        </ul>
      </div>

      <!-- Tarjeta SENSACION TERMICA -->
      <div class="card" style="width: 18rem;">
        <ul class="list-group list-group">
          <li class="list-group-item active" aria-current="true">Sensación Térmica</li>
          <?php
          if($localidad != "") {
            foreach ($xml->prediccion->dia->sens_termica as $sens) {
              // Máxima
              echo '<li class="list-group-item d-flex justify-content-between align-items-start">';
              echo '<div class="ms-2 me-auto">';
              echo "<div class='fw-bold'>Máxima</div>";
              echo "</div>";
              echo '<span class="badge bg-primary rounded-pill">' . $sens->maxima . '&nbsp;Cº</span>';
              // Mínima
              echo '<li class="list-group-item d-flex justify-content-between align-items-start">';
              echo '<div class="ms-2 me-auto">';
              echo "<div class='fw-bold'>Máxima</div>";
              echo "</div>";
              echo '<span class="badge bg-primary rounded-pill">' . $sens->minima . '&nbsp;Cº</span>';
            }
          }
          ?>
        </ul>
      </div>
      <p class="mt-3">Información obtenida de: <?= $productor ?></p>
    </div>

  </div>
</body>

</html>